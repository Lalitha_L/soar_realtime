package com.wissen.security.Entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "workflowlib" ,schema="flightdb")
public class workflowlibrary {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	
	
	@Column(name = "enginefamily")
	private String enginefamily;
	
	@Column(name = "version")
	private String version;

	@Column(name = "description")
	private String description;

	@Column(name = "enginemodel")
	private String engineModel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public String getEnginefamily() {
		return enginefamily;
	}

	public void setEnginefamily(String enginefamily) {
		this.enginefamily = enginefamily;
	}

	
	

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEngineModel() {
		return engineModel;
	}

	public void setEngineModel(String engineModel) {
		this.engineModel = engineModel;
	}

	

	
	

	

}
		
	
	
	
