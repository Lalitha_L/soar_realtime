package com.wissen.security.Entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "analytics" ,schema="flightdb")
public class Analytics {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "Name")
	private String Name;
	@Column(name = "description")
	private String description;
	
	@Column(name = "Language")
	private String language;
	@Column(name = "Author")
	private String author;
	@Column(name = "version")
	private String version;
	@Column(name = "CreateDateTime")
	private  LocalDateTime createdatetime;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public LocalDateTime getCreatedatetime() {
		return createdatetime;
	}
	public void setCreatedatetime(LocalDateTime createdatetime) {
		this.createdatetime = createdatetime;
	}

	

}
