package com.wissen.security.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "search" ,schema="flightdb")
public class Search {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "workflowname")
	private String Workflowname;
	
	@Column(name = "matches")
	private String Matches;
	
	@Column(name = "version")
	private String Version;
	
	@Column(name = "enginemodel")
	private String Enginemodel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getWorkflowname() {
		return Workflowname;
	}

	public void setWorkflowname(String workflowname) {
		Workflowname = workflowname;
	}

	public String getMatches() {
		return Matches;
	}

	public void setMatches(String matches) {
		Matches = matches;
	}

	public String getVersion() {
		return Version;
	}

	public void setVersion(String version) {
		Version = version;
	}

	public String getEnginemodel() {
		return Enginemodel;
	}

	public void setEnginemodel(String enginemodel) {
		Enginemodel = enginemodel;
	}

	public Search() {
		super();
		
	}

	public Search(Integer id, String workflowname, String matches, String version, String enginemodel) {
		super();
		this.id = id;
		Workflowname = workflowname;
		Matches = matches;
		Version = version;
		Enginemodel = enginemodel;
	}


}
