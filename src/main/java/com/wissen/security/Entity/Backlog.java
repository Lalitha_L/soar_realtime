package com.wissen.security.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "backlogdata" ,schema="flightdb")
public class Backlog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "remap")
	private Integer Remap;
	
	@Column(name = "ready")
	private Integer Ready;
	
	@Column(name = "cleanse")
	private Integer Cleanse;
	
	@Column(name = "running")
	private Integer Running;
	
	@Column(name = "numberofuniquetailsinbacklog")
	private Integer Numberunique;
	

	@Column(name = "timeReport")
	private String timeReport;

	public String getTimeReport() {
		return timeReport;
	}

	public void setTimeReport(String timeReport) {
		this.timeReport = timeReport;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRemap() {
		return Remap;
	}

	public void setRemap(Integer remap) {
		Remap = remap;
	}

	public Integer getReady() {
		return Ready;
	}

	public void setReady(Integer ready) {
		Ready = ready;
	}

	public Integer getCleanse() {
		return Cleanse;
	}

	public void setCleanse(Integer cleanse) {
		Cleanse = cleanse;
	}

	public Integer getRunning() {
		return Running;
	}

	public void setRunning(Integer running) {
		Running = running;
	}

	public Integer getNumberunique() {
		return Numberunique;
	}

	public void setNumberunique(Integer numberunique) {
		Numberunique = numberunique;
	}

	public Backlog() {
		super();
		
	}

	public Backlog(Integer remap, Integer ready, Integer cleanse, Integer running, Integer numberunique) {
		super();
		Remap = remap;
		Ready = ready;
		Cleanse = cleanse;
		Running = running;
		Numberunique = numberunique;
	}

//	public void setTimeReport(String format) {
//		// TODO Auto-generated method stub
//		
//	}

	
	
	
}
