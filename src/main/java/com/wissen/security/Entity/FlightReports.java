package com.wissen.security.Entity;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "flightdata" ,schema="flightdb")
public class FlightReports {
	/**
	 * 
	 */
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "status")
	private String status;

	@Column(name = "flightdate")
	private LocalDate loginDate;

	@Column(name = "flighttime")
	private LocalTime loginTime;


	@Column(name = "customername")
	private String customername;
	
	
	@Column(name = "enginefamily")
	private String enginefamily;
	
	@Column(name = "tailnumber")
	private Integer tailnumber;
	
	@Column(name = "last_processed_date")
	private LocalDate lastProcessedDate;
	
	@Column(name = "last_processed_time")
	private LocalTime lastProcessedTime;
	
	@Column(name = "esn")
	private Integer esn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDate getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(LocalDate loginDate) {
		this.loginDate = loginDate;
	}

	public LocalTime getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(LocalTime loginTime) {
		this.loginTime = loginTime;
	}

	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	public String getEnginefamily() {
		return enginefamily;
	}

	public void setEnginefamily(String enginefamily) {
		this.enginefamily = enginefamily;
	}

	public Integer getTailnumber() {
		return tailnumber;
	}

	public void setTailnumber(Integer tailnumber) {
		this.tailnumber = tailnumber;
	}
	public LocalDate getLastProcessedDate() {
		return lastProcessedDate;
	}

	public void setLastProcessedDate(LocalDate lastProcessedDate) {
		this.lastProcessedDate = lastProcessedDate;
	}

	public LocalTime getLastProcessedTime() {
		return lastProcessedTime;
	}

	public void setLastProcessedTime(LocalTime lastProcessedTime) {
		this.lastProcessedTime = lastProcessedTime;
	}

	public Integer getEsn() {
		return esn;
	}

	public void setEsn(Integer esn) {
		this.esn = esn;
	}
	
	

	









}
