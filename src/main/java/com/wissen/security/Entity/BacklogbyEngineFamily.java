package com.wissen.security.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.StringNVarcharType;

@Entity
@Table(name = "BacklogEngineData" ,schema="flightdb")
public class BacklogbyEngineFamily {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "flightnumber")
	private String flightNumber;
	
	@Column(name = "flighttype")
	private String flightType;
	
	@Column(name = "enginefamily")
	private String enginefamily;
	
	private Integer numberOfFlights;

	public Integer getNumberOfFlights() {
		return numberOfFlights;
	}

	public void setNumberOfFlights(Integer numberOfFlights) {
		this.numberOfFlights = numberOfFlights;
	}

	public BacklogbyEngineFamily() {
		super();
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFlightType() {
		return flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getEnginefamily() {
		return enginefamily;
	}

	public void setEnginefamily(String enginefamily) {
		this.enginefamily = enginefamily;
	}






	
}
