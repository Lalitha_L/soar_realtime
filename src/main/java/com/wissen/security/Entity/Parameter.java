package com.wissen.security.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "parameter" ,schema="flightdb")
public class Parameter {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "parameter")
	private String Parameter;
	
	@Column(name = "description")
	private String Description;
	
	@Column(name = "engineModel")
	private String EngineModel;
	
	@Column(name = "dataType")
	private String DataType;
	
	@Column(name = "affliction")
	private String Affliction;
	
	@Column(name = "mapped")
	private String Mapped;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getParameter() {
		return Parameter;
	}

	public void setParameter(String parameter) {
		Parameter = parameter;
	}



	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	
	public String getEngineModel() {
		return EngineModel;
	}

	public void setEngineModel(String engineModel) {
		EngineModel = engineModel;
	}

	public String getDataType() {
		return DataType;
	}

	public void setDataType(String dataType) {
		DataType = dataType;
	}

	public String getAffliction() {
		return Affliction;
	}

	public void setAffliction(String affliction) {
		Affliction = affliction;
	}

	public String getMapped() {
		return Mapped;
	}

	public void setMapped(String mapped) {
		Mapped = mapped;
	}

	public Parameter() {
		super();
	}

	public Parameter(Integer id, String parameter, String description, String engineModel, String dataType,
			String affliction, String mapped) {
		super();
		this.id = id;
		Parameter = parameter;
		Description = description;
		EngineModel = engineModel;
		DataType = dataType;
		Affliction = affliction;
		Mapped = mapped;
	}
}
