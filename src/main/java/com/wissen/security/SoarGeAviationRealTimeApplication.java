package com.wissen.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.wissen.security.Service.DateAndTimeService;
import com.wissen.security.ServiceImpl.DateAndTimeServiceImpl;

@SpringBootApplication
public class SoarGeAviationRealTimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoarGeAviationRealTimeApplication.class, args);
	}
	
	 @Bean
	  public DateAndTimeService dateAndTimeService() {
	    return new DateAndTimeServiceImpl();
	  }
}
