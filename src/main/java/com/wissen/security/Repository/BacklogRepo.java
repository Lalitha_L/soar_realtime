package com.wissen.security.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wissen.security.Entity.Backlog;


public interface BacklogRepo extends JpaRepository<Backlog,Integer> {

}
