package com.wissen.security.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.wissen.security.Entity.Backlog;
import com.wissen.security.Entity.TimeRange;


@Service
public interface DateAneTimeRepository extends JpaRepository<TimeRange,Integer>{

	// @Query("SELECT c FROM TimeRange c WHERE c.timeRange = ?1")
	List<TimeRange> getByTimeRange(String timeRange);

	
	
	

}
