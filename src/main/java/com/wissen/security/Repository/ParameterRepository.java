package com.wissen.security.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.wissen.security.Entity.Parameter;

public interface ParameterRepository extends JpaRepository<Parameter,Integer> {
	@Query("select p from Parameter p where p.Parameter=:keyword OR p.Description=:keyword OR p.EngineModel=:keyword OR DataType=:keyword OR Affliction=:keyword OR Mapped=:keyword ")
	List<Parameter> search(String keyword);


//	@Query("select p from Parameter p where p.Parameter=:keyword OR p.Description=:keyword OR p.EngineModel=:keyword OR DataType=:keyword OR Affliction=:keyword OR Mapped=:keyword ")
//	List<Parameter> search(String keyword);
	
}

