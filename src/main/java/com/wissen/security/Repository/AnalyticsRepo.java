package com.wissen.security.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wissen.security.Entity.Analytics;



public interface AnalyticsRepo extends  JpaRepository<Analytics,Integer>  {

}
