package com.wissen.security.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.wissen.security.Entity.Search;

public interface SearchRepo extends JpaRepository<Search,Integer>{


	@Query("SELECT p FROM Search p WHERE p.Enginemodel  like %:keyword% OR p.Workflowname  like %:keyword% OR p.Version  like %:keyword% OR p.Matches  like %:keyword% ")
	
	
//	@Query("SELECT p FROM Search p WHERE p.Enginemodel=:keyword OR p.Workflowname=:keyword OR p.Version=:keyword OR p.Matches=:keyword ")
	List<Search> search(String keyword);

}
