package com.wissen.security.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wissen.security.Entity.BacklogbyEngineFamily;


public interface BackEngineFamilyRepo extends  JpaRepository<BacklogbyEngineFamily,Integer>  {

}
