package com.wissen.security.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.wissen.security.Entity.FlightReports;
import com.wissen.security.Entity.Parameter;
import com.wissen.security.Entity.workflowlibrary;

@Repository
public interface WorkflowRepo extends JpaRepository<workflowlibrary, Integer>{

	



@Query(value = "select * from workflowlib w  where w.enginemodel like %:keyword%  or  w.enginefamily like %:keyword% ", nativeQuery = true)
List<workflowlibrary> search(String keyword);
//@Query("select p from Parameter p where p.Parameter=:keyword OR p.Description=:keyword OR p.EngineModel=:keyword OR DataType=:keyword OR Affliction=:keyword OR Mapped=:keyword ")
//List<Parameter> search(String keyword);

}
