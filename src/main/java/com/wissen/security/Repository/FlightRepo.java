package com.wissen.security.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.wissen.security.Entity.FlightReports;



public interface FlightRepo extends JpaRepository<FlightReports,Integer> {
	 @Query("SELECT c FROM FlightReports c WHERE c.enginefamily = ?1")
	    public FlightReports findByEngine(String enginefamily);
	 
	 

	
	

}
