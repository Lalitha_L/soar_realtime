package com.wissen.security.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.wissen.security.Entity.Parameter;
import com.wissen.security.Service.ParameterService;

@CrossOrigin(origins = "*", allowedHeaders = "*")

@RestController
@RequestMapping(value = "/par")
public class ParameterController {
		@Autowired
		private ParameterService parameterService;
		
		@PostMapping("/add")
		public Parameter add(@RequestBody Parameter parameter) {
			return parameterService.add(parameter);
		}
		@GetMapping("/getall")
		public List<Parameter> getAll() {
			return parameterService.getAll();
		}
		@GetMapping("/get/{id}")
		public Parameter getById(@PathVariable Integer id) {
			return parameterService.getById(id);
		}
		@DeleteMapping("/deleteparameter/{id}")
		public void deleteParameter(@PathVariable int id) {
			parameterService.removeParameter(id);

		}
	

		@GetMapping("/fliter")
		public List<Parameter> home(@RequestParam("keyword") String keyword){
			List<Parameter> listparam = parameterService.listAll(keyword);
			return listparam;
	}
}