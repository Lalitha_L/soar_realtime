package com.wissen.security.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wissen.security.Entity.Parameter;
import com.wissen.security.Entity.workflowlibrary;
import com.wissen.security.Repository.WorkflowRepo;
import com.wissen.security.Service.FlightService;
import com.wissen.security.Service.WorkflowService;

import org.springframework.ui.Model;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/workflowlib")
public class WorkflowController {
	
	@Autowired
	private WorkflowService workflowService;
	@Autowired
	private FlightService flightService;
	@Autowired
	private WorkflowRepo workflowrepo;
	
//	@Autowired
//	private FlightRepo flightrepo;
	
	
	@PostMapping("/add")
	public workflowlibrary add(@RequestBody workflowlibrary workflowlib) {
		
		
		
		
		
//		workflowlibrary loginhis =new workflowlibrary();
//		loginhis.setEnginefamily(loginhis.getEnginefamily());
//		
//		loginhis.setDescription(loginhis.getDescription());
//		loginhis.setVersion(loginhis.getVersion());
//		loginhis.setEngineModel(loginhis.getEngineModel());
//		workflowrepo.save(loginhis);
		return workflowService.add(workflowlib);
	}

	@GetMapping("/getall")
	public List<workflowlibrary> getAlldata() {
		return workflowService.getAll();
	}
	
	@GetMapping("/get/{id}")
	public workflowlibrary getById(@PathVariable Integer id) {
		return workflowService.getById(id);
	}
	@DeleteMapping("/deleteworkflowlibrary/{id}")
	public void deleteParameter(@PathVariable int id) {
		workflowService.removeworkflowlibrary(id);

	}
	
//	@RequestMapping("/search")
//	public List<workflowlibrary> home(@RequestParam("keyword") String keyword){
//		List<workflowlibrary> workflow = workflowService.listAll(keyword);
//		return workflow;
//}
	@GetMapping("/filter")
	public List<workflowlibrary> home(@RequestParam("keyword") String keyword){
		List<workflowlibrary> listworkflow = workflowService.listAll(keyword);
		return listworkflow;

}
}

