package com.wissen.security.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wissen.security.Entity.BacklogbyEngineFamily;
import com.wissen.security.Service.BacklogbyEngineFamilyService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/BacklogEngine")
public class BacklogbyEngineFamilyController {
	
	@Autowired
	private BacklogbyEngineFamilyService service;
	
	@PostMapping("/add")
	public BacklogbyEngineFamily add(@RequestBody BacklogbyEngineFamily backlogbyEngineFamily) {
	
		return service.add(backlogbyEngineFamily);
	}
	
	@GetMapping("/getall")
	public List<BacklogbyEngineFamily> getAlldata() {
		return service.getAll();
	}
	
	@GetMapping("/get/{id}")
	public BacklogbyEngineFamily getById(@PathVariable Integer id) {
		return service.getById(id);
	}

}
