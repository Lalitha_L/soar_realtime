package com.wissen.security.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wissen.security.Entity.Search;
import com.wissen.security.Service.SearchService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/search")
public class SearchController {
	
	@Autowired
	private SearchService searchService;
	
	
	@PostMapping("/add")
	public Search add(@RequestBody Search search) {
		return searchService.add(search);
	}

	
	@GetMapping("/getall")
	public List<Search> getAlldata() {
		return searchService.getAll();
	}
	
	

	@GetMapping("/get/{id}")
	public Search getById(@PathVariable Integer id) {
		return searchService.getById(id);
	}
	

	@DeleteMapping("/delete/{id}")
	public String deleteUser(@PathVariable Integer id) {
		searchService.deleteById(id);
	return "Deleted success";
	}
	
	
	 @GetMapping("/fliter")
	    public List<Search> viewHomePage(@RequestParam("keyword") String keyword) {
	        List<Search> listSearch = searchService.listAll(keyword);
	       return listSearch;
	    }
}
