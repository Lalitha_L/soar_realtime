package com.wissen.security.Controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wissen.security.Entity.FlightReports;
import com.wissen.security.Service.FlightService;



@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/flight")

public class FlightController {

	@Autowired
	private FlightService flightService;


	@PostMapping("/add")
	public FlightReports add(@RequestBody FlightReports flightReports) {
		flightReports.setLoginDate(LocalDate.now());
		flightReports.setLoginTime(LocalTime.now());
		flightReports.setLastProcessedDate(LocalDate.now());
		flightReports.setLastProcessedTime(LocalTime.now());
		return flightService.add(flightReports);
	}
	
	@GetMapping("/getall")
	public List<FlightReports> getAlldata() {
		return flightService.getAll();
	}
	
	@GetMapping("/get/{id}")
	public FlightReports getById(@PathVariable Integer id) {
		return flightService.getById(id);
	}
	
	@DeleteMapping("/delete/{id}")
	public String deleteUser(@PathVariable Integer id) {
		flightService.deleteById(id);
	return "Deleted success";
	}
	
	

}
