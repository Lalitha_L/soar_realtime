package com.wissen.security.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wissen.security.Entity.Analytics;
import com.wissen.security.Service.AnalyticsService;

@CrossOrigin
@RestController
@RequestMapping(value = "/Analytics")
public class AnalyticsController {
	
	@Autowired
	private AnalyticsService anaservice;
	
	@PostMapping("/add")
	public Analytics add(@RequestBody Analytics analytics) {
	
		return anaservice.add(analytics);
	}
	
	@GetMapping("/getall")
	public List<Analytics> getAlldata() {
		return anaservice.getAll();
	}
	
	@GetMapping("/get/{id}")
	public Analytics getById(@PathVariable Integer id) {
		return anaservice.getById(id);
	}
	@DeleteMapping("/deleteanalytics/{id}")
	public void deleteanalytics(@PathVariable int id) {
		anaservice.removeanalytics(id);

	}
}
