package com.wissen.security.Controller;


import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wissen.security.Entity.Backlog;
import com.wissen.security.Entity.FlightReports;
import com.wissen.security.Entity.TimeRange;
import com.wissen.security.Repository.DateAneTimeRepository;
import com.wissen.security.Repository.FlightRepo;
import com.wissen.security.Service.BacklogService;
import com.wissen.security.Service.DateAndTimeService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/backlog")
public class BacklogController {
	
	@Autowired	
    private DateAndTimeService dateAneTimeService;
	
	@Autowired
	private BacklogService backlogService;
	
	@Autowired
	private DateAneTimeRepository dateAneTimeRepository;
	
	@Autowired
	private FlightRepo flightRepo;
	
	
	@PostMapping("/add")
	public Backlog add(@RequestBody Backlog backlog) {
		SimpleDateFormat sd = new SimpleDateFormat(
	            "HH:mm:SS z");
	        Date date = new Date();
	        // TODO: Avoid using the abbreviations when fetching time zones.
	        // Use the full Olson zone ID instead.
	        sd.setTimeZone(TimeZone.getTimeZone("IST"));
	        System.out.println(sd.format(date));

	
	        backlog.setTimeReport(sd.format(date));
//
//		return	dateAneTimeRepository.save(time);
		return backlogService.add(backlog);
	}
	@GetMapping("/getall")
	public List<Backlog> getAlldata() {
		return backlogService.getAll();
	}
	
	@GetMapping("/get/{id}")
	public Backlog getById(@PathVariable Integer id) {
		return backlogService.getById(id);
	}
//	@PostMapping("/time")
//	public TimeEntity addDateAndTime() {
//		TimeEntity time = new TimeEntity();
//		SimpleDateFormat sd = new SimpleDateFormat(
//	            "dd-MM-yy HH:mm:SS z");
//	        Date date = new Date();
//	        // TODO: Avoid using the abbreviations when fetching time zones.
//	        // Use the full Olson zone ID instead.
//	        sd.setTimeZone(TimeZone.getTimeZone("IST"));
//	        System.out.println(sd.format(date));
//
//	
//			time.setDate(sd.format(date));
//
//		return	dateAneTimeRepository.save(time);
// 
//}
	@GetMapping("/time")
	public LocalTime getTime(HttpServletRequest request, @RequestParam("enginefamily") String enginefamily) {
		
		FlightReports flightReports = flightRepo.findByEngine(request.getParameter("enginefamily"));
		
		
		return  flightReports.getLoginTime();
		
	}
	@PostMapping("/addTimeRange")
	public TimeRange addTimeRange(@RequestBody TimeRange timeEntity) {
		timeEntity.setTime(LocalTime.now());
		return dateAneTimeRepository.save(timeEntity);
	}
	
	@GetMapping("/timeRange")
	public List<TimeRange> getByTimeRange(@RequestParam("timeRange") String timeRange) {
		return (List<TimeRange>) dateAneTimeRepository.getByTimeRange(timeRange);
	}
	
	@GetMapping("/allTimeRange")
	public List<TimeRange> getAllTimeRange() {
		return (List<TimeRange>) dateAneTimeService.getall();
	}
	
}
