package com.wissen.security.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.wissen.security.ServiceImpl.BacklogServiceImpl;
@EnableWebSecurity
public class MvcConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private BacklogServiceImpl customUserDetailsService;



	

	@Bean
	CorsConfigurationSource corsConfigurationSource()
	{
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList("*"));
		configuration.setAllowedOrigins(Arrays.asList("http://15.207.85.90:8080"));
		configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		super.configure(auth);
	
	}



	@Override
	public void configure(HttpSecurity security) throws Exception {
		security.cors().disable().csrf().disable()
		//security.csrf().disable()
		.authorizeRequests()
		.antMatchers("/authenticate", "/signin").permitAll()
		.anyRequest().authenticated().and();
		//security.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**",
				"/configuration/security", "/swagger-ui.html","/backlog/timeRange**"
				, "/webjars/**","/verify/**","/BacklogEngine/getall/**","/flight/getall/**","/par/getall/**"
				,"/searchProfileBy/{Id}/**","/search/getall/**","/search/fliter/**","/workflowlib/getall/**","/workflowlib/search/{enginefamily}/**"
				,"/forgot_password","/workflowlib/search/{enginemodel}/**","/par/search/**",  "/workflowlib/filter/**","/par/add/**",
						"/par/get/{id}/**","/par/filter/**","/workflowlib/get/{id}/**","/workflowlib/add/**",
								 "/backlog/getall/**","/Analytics/add/**","/Analytics/getall/**","/Analytics/get/{id}/**"
								 ,"/Analytics/deleteanalytics/{id}/**","/workflowlib/deleteworkflowlibrary/{id}/**","/backlog/allTimeRange/**",
				"/searchProfileBy/{Id}/**","/search/add","/search/delete/{id}","/search/fliter/**","/workflowlib/getall/**","/flight/add","/flight/get/{id}","/flight/delete/{id}","/workflowlib/search/{enginefamily}/**"
				,"/forgot_password","/workflowlib/search/{enginemodel}/**","/backlog/addTimeRange/**","/backlog/add/**");
	}
	


	@Override
	@Bean
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	


}



