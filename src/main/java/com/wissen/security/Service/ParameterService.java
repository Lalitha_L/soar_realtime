package com.wissen.security.Service;

import java.util.List;

import com.wissen.security.Entity.Parameter;

public interface ParameterService {

	public Parameter add(Parameter ur);

	//public Parameter update(Parameter ur);

	public List<Parameter> getAll();

//	public void deleteById(int id);

	//public Parameter getById(Integer id);

	//public void removeParameter(int id);

	Parameter getById(Integer id);

	public void removeParameter(int id);

	List<Parameter> listAll(String keyword);

}
