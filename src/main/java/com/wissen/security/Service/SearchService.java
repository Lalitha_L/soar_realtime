package com.wissen.security.Service;

import java.util.List;

import com.wissen.security.Entity.Search;

public interface SearchService {

	Search add(Search search);

	List<Search> getAll();

	Search getById(Integer id);

	void deleteById(Integer id);

	List<Search> listAll(String keyword);



}
