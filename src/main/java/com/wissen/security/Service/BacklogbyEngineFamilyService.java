package com.wissen.security.Service;

import java.util.List;

import com.wissen.security.Entity.BacklogbyEngineFamily;

public interface BacklogbyEngineFamilyService {

	BacklogbyEngineFamily add(BacklogbyEngineFamily backlogbyEngineFamily);

	List<BacklogbyEngineFamily> getAll();

	BacklogbyEngineFamily getById(Integer id);



}
