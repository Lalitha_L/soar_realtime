package com.wissen.security.Service;

import java.util.List;

import com.wissen.security.Entity.Backlog;

public interface BacklogService {

	Backlog add(Backlog backlog);

	List<Backlog> getAll();

	Backlog getById(Integer id);

}
