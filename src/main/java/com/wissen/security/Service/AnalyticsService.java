package com.wissen.security.Service;

import java.util.List;

import com.wissen.security.Entity.Analytics;

public interface AnalyticsService {

	Analytics add(Analytics analytics);

	List<Analytics> getAll();

	Analytics getById(Integer id);

	void removeanalytics(int id);
}
