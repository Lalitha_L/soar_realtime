package com.wissen.security.Service;

import java.util.List;

import com.wissen.security.Entity.FlightReports;

public interface FlightService {

	FlightReports add(FlightReports flightReports);

	List<FlightReports> getAll();

	FlightReports getById(Integer id);

	void deleteById(Integer id);

}
