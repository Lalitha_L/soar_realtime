package com.wissen.security.ServiceImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wissen.security.Entity.workflowlibrary;
import com.wissen.security.Repository.WorkflowRepo;
import com.wissen.security.Service.WorkflowService;

@Service
public class WorkflowServiceImpl implements WorkflowService {

	@Autowired
	private WorkflowRepo workflowRepo;

	@Override
	public workflowlibrary add(workflowlibrary workFlow) {
		return workflowRepo.save(workFlow);
	}

	@Override
	public List<workflowlibrary> getAll() {

		return workflowRepo.findAll();
	}

	@Override
	public workflowlibrary getById(Integer id) {
		return workflowRepo.findById(id).get();
	}

	@Override
	public void removeworkflowlibrary(int id) {
		workflowRepo.deleteById(id);
		
	}

	@Override
	public List<workflowlibrary> listAll(String keyword) {
		
			if (keyword != null) {
				return workflowRepo.search(keyword);
			}
			return workflowRepo.findAll();
	}



}
