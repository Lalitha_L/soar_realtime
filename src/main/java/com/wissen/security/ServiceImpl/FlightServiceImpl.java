package com.wissen.security.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wissen.security.Entity.FlightReports;
import com.wissen.security.Repository.FlightRepo;
import com.wissen.security.Service.FlightService;



@Service
public class FlightServiceImpl implements FlightService {
	
	@Autowired
	private FlightRepo flightRepo;

	@Override
	public FlightReports add(FlightReports flightReports) {	
		return flightRepo.save(flightReports);
	}

	@Override
	public List<FlightReports> getAll() {
		return flightRepo.findAll();
	}

	@Override
	public FlightReports getById(Integer id) {
		return flightRepo.findById(id).get();
	}


	@Override
	public void deleteById(Integer id) {
		flightRepo.deleteById(id);
	}


}
