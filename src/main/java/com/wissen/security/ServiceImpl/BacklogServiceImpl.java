package com.wissen.security.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wissen.security.Entity.Backlog;
import com.wissen.security.Repository.BacklogRepo;
import com.wissen.security.Service.BacklogService;

@Service
public class BacklogServiceImpl implements BacklogService{
	
	@Autowired
	private BacklogRepo backlogRepo;

	@Override
	public Backlog add(Backlog backlog) {
		return backlogRepo.save(backlog);	
	}

	@Override
	public List<Backlog> getAll() {
		return backlogRepo.findAll();
	}

	@Override
	public Backlog getById(Integer id) {
		
		return backlogRepo.findById(id).get();
	}

}
