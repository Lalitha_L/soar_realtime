package com.wissen.security.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wissen.security.Entity.Parameter;
import com.wissen.security.Repository.ParameterRepository;
import com.wissen.security.Service.ParameterService;

@Service
public  class ParameterServiceImpl implements ParameterService{
	
	@Autowired
	private ParameterRepository parameterRepo;

	@Override
	public Parameter add(Parameter parameter) {
		return parameterRepo.save(parameter);	
	}

	@Override
	public List<Parameter> getAll() {
		return parameterRepo.findAll();
	}
	@Override
	public Parameter getById(Integer id) {
		return parameterRepo.findById(id).get();
	}


	@Override
	public void removeParameter(int id) {
		parameterRepo.deleteById(id);
	}

	
	@Override
	public List<Parameter> listAll(String keyword) {
		if(keyword!=null) {
			return parameterRepo.search(keyword);
		}
		return null;	
		}
	}
	

