package com.wissen.security.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wissen.security.Entity.BacklogbyEngineFamily;
import com.wissen.security.Repository.BackEngineFamilyRepo;
import com.wissen.security.Service.BacklogbyEngineFamilyService;


@Service
public class BacklogbyEngineFamileServiceImpl implements BacklogbyEngineFamilyService {
	
	@Autowired
	private BackEngineFamilyRepo  engineRepo;

	@Override
	public BacklogbyEngineFamily add(BacklogbyEngineFamily backlogbyEngineFamily) {
	return engineRepo.save(backlogbyEngineFamily);
	}

	@Override
	public List<BacklogbyEngineFamily> getAll() {
		return engineRepo.findAll();
	}

	@Override
	public BacklogbyEngineFamily getById(Integer id) {
		// TODO Auto-generated method stub
		return engineRepo.findById(id).get();
	}

}
