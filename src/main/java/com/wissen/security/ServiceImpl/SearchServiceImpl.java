package com.wissen.security.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wissen.security.Entity.Search;
import com.wissen.security.Repository.SearchRepo;
import com.wissen.security.Service.SearchService;

@Service
public class SearchServiceImpl implements SearchService{

	@Autowired
	private SearchRepo searchRepo;

	@Override
	public Search add(Search search) {
	return searchRepo.save(search);
	}

	@Override
	public List<Search> getAll() {
		return searchRepo.findAll();
	}

	@Override
	public Search getById(Integer id) {
		return searchRepo.findById(id).get();
	}

	@Override
	public void deleteById(Integer id) {
		searchRepo.deleteById(id);
	}
	
//	@Override
//	   public List<Search> listAll(String keyword) {
//	        if (keyword != null) {
//	            return searchRepo.search(keyword);
//	        }
//	        return null;
//	    }
//	     
	@Override
	   public List<Search> listAll(String keyword) {
	        if (keyword != null) {
	            return searchRepo.search(keyword);
	        }
	        return null;
	    }
	     

}
