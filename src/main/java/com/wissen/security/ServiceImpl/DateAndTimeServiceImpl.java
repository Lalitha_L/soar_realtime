package com.wissen.security.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.wissen.security.Entity.TimeRange;
import com.wissen.security.Repository.DateAneTimeRepository;
import com.wissen.security.Service.DateAndTimeService;

public class DateAndTimeServiceImpl  implements DateAndTimeService{
	
	@Autowired
	DateAneTimeRepository dateAneTimeRepository;

	@Override
	public List<TimeRange> getall() {
		return dateAneTimeRepository.findAll();
	}

}
