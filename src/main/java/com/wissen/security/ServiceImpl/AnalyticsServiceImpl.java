package com.wissen.security.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wissen.security.Entity.Analytics;
import com.wissen.security.Repository.AnalyticsRepo;
import com.wissen.security.Service.AnalyticsService;

@Service
public class AnalyticsServiceImpl implements AnalyticsService {
	
	@Autowired
	private AnalyticsRepo  analyticsrepo;

	@Override
	public Analytics add(Analytics analytics) {
		
		return analyticsrepo.save(analytics);
	}

	@Override
	public List<Analytics> getAll() {
		
		return analyticsrepo.findAll();
	}

	@Override
	public Analytics getById(Integer id) {
		
		return analyticsrepo.findById(id).get();
	}

	@Override
	public void removeanalytics(int id) {
		
		analyticsrepo.deleteById(id);
	}
	
	
}