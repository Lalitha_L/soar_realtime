
package com.wissen.security;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.wissen.security.Entity.Search;
import com.wissen.security.Repository.SearchRepo;




public class SearchTestService extends SoarGeAviationRealTimeApplicationTests  {

	@MockBean
	private SearchRepo searchRepo;




	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

//	@BeforeEach
//	public void setup() {
//		search = new Search(1, "ball", "cat", "dhhj", "coffee");
//		mvc = MockMvcBuilders.standaloneSetup(searchController).build();
//	}
//
//	@AfterEach
//	void tearDown() {
//		search = null;
//	}
//
//	@Test
//	public void PostMappingOfCourse() throws Exception {
//		when(backlogService.add(any())).thenReturn(search);
//		mvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(asJsonString(search)));
//	
//	}
	
//	  
	
	@Test
	public void should_find_Search_by_id() {
		// Given
		Search user = new Search();
		when(searchRepo.findById(null)).thenReturn(Optional.of(user));
		//when(basicRepositoryMock.findById(ID)).thenThrow(ResourceNotFoundException.class);
		
		// When
		// Then
	//	assertEquals(user, JwtService.getId(ID));
	}

	@Test
	public void should_delete_Search_by_id() {
		// Given
		Search user = new Search();
		when(searchRepo.findById(user.getId())).thenReturn(Optional.of(user));
		// When
		doNothing().when(searchRepo).deleteById(null);
		// Then
		//assertEquals(user, jwtUserRepository.deleteById(ID));
	}
	@Test
	public void should_add_SearchData() throws Exception {
		Search user = new Search();
		given(searchRepo.save(any(Search.class))).willReturn(user);

	 //  assertEquals(1, jwtUserRepository.findById(1));

	}
	
	@Test
	public void should_get_all_searchData() {
		Search user = new Search();
		List<Search> basics = Collections.singletonList(user);
		when(searchRepo.findAll()).thenReturn(basics);
	
	}





}