package com.wissen.security.Service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.wissen.security.Entity.Analytics;
import com.wissen.security.Repository.AnalyticsRepo;

@SpringBootTest
public class AnalyticsServiceTest {

	@MockBean
	private AnalyticsRepo  analyticsrepo;

	private MockMvc mvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void add() throws Exception {
		Analytics ann = new Analytics  ();
		
		given( analyticsrepo.save(any(Analytics.class))).willReturn(ann);
	}

	@Test
	public void findbyid() throws Exception{

		Analytics ann = new Analytics  ();
		when(analyticsrepo.findById(null)).thenReturn(Optional.of(ann));
	}

	@Test
	public void deletebyid() throws Exception {

		Analytics ann = new Analytics  ();
		when(analyticsrepo.findById(ann.getId())).thenReturn(Optional.of(ann));
		doNothing().when(analyticsrepo).deleteById(null);

	}
	@Test
	public void findall() throws Exception {
		Analytics ann = new Analytics  ();
		List<Analytics> basics = Collections.singletonList(ann);
		when(analyticsrepo.findAll()).thenReturn(basics);
		
	}
}
