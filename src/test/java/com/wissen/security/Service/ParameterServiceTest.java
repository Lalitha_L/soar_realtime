package com.wissen.security.Service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.wissen.security.Entity.Parameter;
import com.wissen.security.Repository.ParameterRepository;

@SpringBootTest
public class ParameterServiceTest {

	@MockBean
	private ParameterRepository parameterRepository;

	private MockMvc mvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void add() throws Exception {

		Parameter par = new Parameter();
		given(parameterRepository.save(any(Parameter.class))).willReturn(par);
	}

	@Test
	public void findbyid() throws Exception{

		Parameter par = new Parameter();
		when(parameterRepository.findById(null)).thenReturn(Optional.of(par));
	}

	@Test
	public void deletebyid() throws Exception {

		Parameter par = new Parameter();
		when(parameterRepository.findById(par.getId())).thenReturn(Optional.of(par));
		doNothing().when(parameterRepository).deleteById(null);

	}
	@Test
	public void findall() throws Exception {
		Parameter par = new Parameter();
		List<Parameter> basics = Collections.singletonList(par);
		when(parameterRepository.findAll()).thenReturn(basics);
		
	}
}