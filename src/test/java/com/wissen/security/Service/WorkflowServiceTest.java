package com.wissen.security.Service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.wissen.security.Entity.Parameter;
import com.wissen.security.Entity.workflowlibrary;
import com.wissen.security.Repository.ParameterRepository;
import com.wissen.security.Repository.WorkflowRepo;

@SpringBootTest
public class WorkflowServiceTest {

	@MockBean
	private WorkflowRepo workflowrepo;

	private MockMvc mvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void add() throws Exception {

		workflowlibrary work = new workflowlibrary();
		given( workflowrepo.save(any(workflowlibrary.class))).willReturn(work);
	}

	@Test
	public void findbyid() throws Exception{

		workflowlibrary work = new workflowlibrary();
		when( workflowrepo.findById(null)).thenReturn(Optional.of(work));
	}

	@Test
	public void deletebyid() throws Exception {

		workflowlibrary work = new workflowlibrary();
		when(workflowrepo.findById(work.getId())).thenReturn(Optional.of(work));
		doNothing().when(workflowrepo).deleteById(null);

	}
	@Test
	public void findall() throws Exception {
		workflowlibrary work = new workflowlibrary();
		List<workflowlibrary> basics = Collections.singletonList(work);
		when(workflowrepo.findAll()).thenReturn(basics);
		
	}
}
