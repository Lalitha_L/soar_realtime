package com.wissen.security;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.wissen.security.Entity.FlightReports;
import com.wissen.security.Repository.FlightRepo;








public class FlightTestService extends SoarGeAviationRealTimeApplicationTests {

	@MockBean
	private FlightRepo flightRepo;

	private MockMvc mvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void should_add_FlightData() throws Exception {
		
		FlightReports user = new FlightReports();
		given(flightRepo.save(any(FlightReports.class))).willReturn(user);
	}
	
	@Test
	public void should_find_FlightData_by_id() throws Exception{
	
		FlightReports user = new FlightReports();
		when(flightRepo.findById(null)).thenReturn(Optional.of(user));
	}

	@Test
	public void should_delete_FlightData_by_id() throws Exception {
	
		FlightReports user = new FlightReports();
		when(flightRepo.findById(user.getId())).thenReturn(Optional.of(user));
	    doNothing().when(flightRepo).deleteById(null);
		
	}
	@Test
	public void should_find_all_FlightData() throws Exception {
		// Given
		FlightReports user = new FlightReports();
		List<FlightReports> basics = Collections.singletonList(user);
		when(flightRepo.findAll()).thenReturn(basics);

	}



}
