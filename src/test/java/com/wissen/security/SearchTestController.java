package com.wissen.security;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.wissen.security.Entity.Search;
import com.wissen.security.Repository.SearchRepo;
import com.wissen.security.Service.SearchService;


public class SearchTestController extends SoarGeAviationRealTimeApplicationTests  {
	
	@Autowired
	SearchService searchService;
	@Autowired
	SearchRepo searchRepo;
	private static final byte[] outputInJson = null;
	
	@Test
	public void testSearchadd() throws Exception {
    
	Search ser = new Search();
	ser.getId();
	ser.getEnginemodel();
	ser.getMatches();
	ser.getVersion();
	ser.getWorkflowname();
	
	String URI = "//search/add";
	RequestBuilder requestBuilder = MockMvcRequestBuilders
	.post(URI)
	.accept(MediaType.APPLICATION_JSON).content(outputInJson)
	.contentType(MediaType.APPLICATION_JSON);
	}
	
	@Test
	public void testgetById() throws Exception {
	Search ser = new Search();
	ser.setId(3);
	ser.setEnginemodel("EngineFam3");
	ser.setMatches("CFO45");
	ser.setVersion("005");
	ser.setWorkflowname("work failed");

	String URI = "/search/getall";
	}

}
