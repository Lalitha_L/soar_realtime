package com.wissen.security.Controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.wissen.security.Entity.Analytics;
import com.wissen.security.Repository.AnalyticsRepo;
import com.wissen.security.Service.AnalyticsService;
class AnalyticsControllerTest {
@Autowired
AnalyticsService anaservice;
@Autowired
AnalyticsRepo  analyticsrepo;
private static final byte[] outputInJson = null;
	
	@Test
	public void testadd() throws Exception {
		
		Analytics ann = new Analytics  ();
		ann.getAuthor();
		ann.getCreatedatetime();
		ann.getDescription();
		ann.getId();
		ann.getLanguage();
		ann.getName();
		ann.getVersion();
		
		String URI = "//Analytics/add";		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(URI)
				.accept(MediaType.APPLICATION_JSON).content(outputInJson)
				.contentType(MediaType.APPLICATION_JSON);
}
	
	@Test
	public void testgetById() throws Exception {
		Analytics ann = new Analytics  ();
	ann.setAuthor("flight");
	ann.setDescription("good");
	ann.setId(1);
	ann.setLanguage("java");
	ann.setName("core");
	ann.setVersion("40");
	
		
		String URI = "//Analytics/get/{id}";		
	}
	
	@Test
	public void testgetAll() throws Exception {
		Analytics ann = new Analytics  ();
		
		ann.setAuthor("flight1");
		ann.setDescription("good1");
		ann.setId(1);
		ann.setLanguage("java1");
		ann.setName("core1");
		ann.setVersion("9.0");
		
		String URI = "//Analytics/getall";		
	}
}