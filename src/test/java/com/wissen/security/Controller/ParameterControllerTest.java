package com.wissen.security.Controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.wissen.security.Entity.Parameter;
import com.wissen.security.Repository.ParameterRepository;
import com.wissen.security.Service.ParameterService;

class ParameterControllerTest {
@Autowired
ParameterService parameterService;
@Autowired
ParameterRepository parameterRepository;
private static final byte[] outputInJson = null;
	
	@Test
	public void testadd() throws Exception {
		
		Parameter par = new Parameter();
		par.getId();
		par.getDescription();
		par.getEngineModel();
		par.getDataType();
		par.getAffliction();
		par.getMapped();
		
		String URI = "/par/add";		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(URI)
				.accept(MediaType.APPLICATION_JSON).content(outputInJson)
				.contentType(MediaType.APPLICATION_JSON);
}
	
	@Test
	public void testgetById() throws Exception {
		Parameter par = new Parameter();
		par.setId(1);
		par.setDescription("data");
		par.setEngineModel("18451");
		par.setDataType("int");
		par.setAffliction("bcudsuf");
		par.setMapped("done");
		par.getId();
		
		String URI = "/par/get/{id}";		
	}
	
	@Test
	public void testgetAll() throws Exception {
		Parameter par = new Parameter();
		par.setId(2);
		par.setDescription("db");
		par.setEngineModel("654841");
		par.setDataType("Long");
		par.setAffliction("hvt21");
		par.setMapped("done");
		
		String URI = "/par/getall";		
	}
	
//	@Test
//	public void deletePatientById_success() throws Exception {
//	    Mockito.when(parameterRepository.findById(2)).thenReturn(null);
//
//	    mockMvc.perform(MockMvcRequestBuilders
//	            .delete("/patient/2")
//	            .contentType(MediaType.APPLICATION_JSON))
//	            .andExpect(status().isOk());
//	}
	}