package com.wissen.security.Controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.wissen.security.Entity.workflowlibrary;
import com.wissen.security.Repository.WorkflowRepo;
import com.wissen.security.Service.WorkflowService;

class WorkflowControllerTest {
@Autowired
WorkflowService workflowService;
@Autowired
WorkflowRepo workflowrepo;
private static final byte[] outputInJson = null;
	
	@Test
	public void testadd() throws Exception {
		
		workflowlibrary work = new workflowlibrary  ();
		work.getId();
		work.getDescription();
		work.getEnginefamily();
		work.getEngineModel();
		work.getVersion();
	
		
		String URI = "//workflowlib/add";		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(URI)
				.accept(MediaType.APPLICATION_JSON).content(outputInJson)
				.contentType(MediaType.APPLICATION_JSON);
}
	
	@Test
	public void testgetById() throws Exception {
		workflowlibrary work = new workflowlibrary  ();
		work.setId(1);
		work.setDescription("data");
		work.setEnginefamily("Engfam-1");
		work.setEngineModel("GE30");
		work.setVersion("8");
	
		
		
		String URI = "//workflowlib/get/{id}";		
	}
	
	@Test
	public void testgetAll() throws Exception {
		workflowlibrary work = new workflowlibrary  ();
		work.setId(1);
		work.setDescription("data");
		work.setEnginefamily("Engfam-2");
		work.setEngineModel("GE90");
		work.setVersion("7.0");
	
		
		String URI = "//workflowlib/getall";		
	}
}